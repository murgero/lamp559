# LAMP Stack Cloudron App

This repository contains the Cloudron app package source for a plain LAMP stack.

### Specs:

- Ubuntu 14.04
- PHP 5.5.9
- phpMyAdmin
- SFTP


**THIS REPO AND APP SHOULD NOT BE USED IN PRODUCTION. THIS APP IS NOT SECURE AND IS NOT SAFE TO RUN IN PRODUCTION ENVIRONMENTS**

## Installation

```
git clone https://git.cloudron.io/murgero/lamp559
cd lamp559
cloudron login
cloudron install --image=mitchellurgero/org.urgero.lamp559:latest
```

## Usage

Use `cloudron push` to copy files into `/app/data/public/` and `cloudron exec` to get a remote terminal.

See https://cloudron.io/references/cli.html for how to get the `cloudron` command line tool.


## Tests

* Put `HashKnownHosts no` in your `~/.ssh/config`
* cd test
* npm install
* USERNAME=<> PASSWORD=<> mocha --bail test.js

