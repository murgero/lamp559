This app only supports <upstream>PHP 7.2</upstream>

## Cloudron LAMP Stack

LAMP is an archetypal model of web service stacks, named as an acronym of the names of its original four open-source components: the Linux operating system,
the Apache HTTP Server, the MySQL relational database management system (RDBMS), and the PHP programming language.

### Cron

This app supports running one or more cronjobs. The jobs are specified using the standard crontab syntax.

## ionCube

ionCube is a PHP module extension that loads encrypted PHP files and speeds up webpages. ionCube is pre-installed
and enabled by default.

### Remote Terminal

Use the [web terminal](https://cloudron.io/documentation/apps/#web-terminal) for a remote shell connection into the
app to adjust configuration files like `php.ini`.


### Using Multi-DB

```
root@9bbac34d-452d-49ab-beac-a90f90549085:/app/data# mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST}
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 2701
Server version: 5.7.29-0ubuntu0.18.04.1 (Ubuntu)

mysql> CREATE DATABASE `e26ceb5eff7b17b8_db1`;
Query OK, 1 row affected (0.00 sec)

mysql> CREATE DATABASE `e26ceb5eff7b17b8_db2`;
Query OK, 1 row affected (0.00 sec)

mysql> SHOW DATABASES;
+----------------------+
| Database             |
+----------------------+
| information_schema   |
| e26ceb5eff7b17b8_db1 |
| e26ceb5eff7b17b8_db2 |
+----------------------+
3 rows in set (0.00 sec)
```
