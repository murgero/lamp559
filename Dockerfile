FROM dimonpvt/php5.5.9:latest

RUN mkdir -p /app/code /app/data
WORKDIR /app/code



RUN apt-get update && apt-get install -y crudini cron curl sendmail mysql-client wget redis-tools && \
    apt-get -y install apache2-dev apache2-utils build-essential g++ libxml2-dev libxslt1-dev && \
    apt-get -y install libfcgi-dev libfcgi0ldbl libjpeg62-dbg libxml2-dev && \
    apt-get -y install libmcrypt-dev libssl-dev libc-client2007e libc-client2007e-dev && \
    apt-get -y install libbz2-dev libcurl4-openssl-dev libjpeg-dev libpng12-dev && \
    apt-get -y install libfreetype6-dev libkrb5-dev libpq-dev libicu-dev libjemalloc1 libjemalloc-dev gcc make

## Redis Client
# RUN cd /tmp && \
#     wget http://download.redis.io/redis-stable.tar.gz && \
#     tar xvzf redis-stable.tar.gz && \
#     cd redis-stable && \
#     make && \
#     cp src/redis-cli /usr/local/bin/ && \
#     chmod 755 /usr/local/bin/redis-cli

## Fix for start.sh and phpmyAdmin
RUN apt-get install pwgen -y

## Fix for Ubnt 14.04 not having supervisor by default
RUN apt-get install supervisor -y

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf
COPY apache/lamp.conf /etc/apache2/sites-enabled/lamp.conf
RUN echo "Listen 80" > /etc/apache2/ports.conf
RUN a2disconf other-vhosts-access-log
RUN a2enmod rewrite headers rewrite expires cache

# configure mod_php
RUN crudini --set /etc/php5/apache2/php.ini PHP upload_max_filesize 2512M && \
    crudini --set /etc/php5/apache2/php.ini PHP post_max_size 2512M && \
    crudini --set /etc/php5/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php5/apache2/php.ini Session session.save_path /run/app/sessions && \
    crudini --set /etc/php5/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php5/apache2/php.ini Session session.gc_divisor 100

RUN cp /etc/php5/apache2/php.ini /app/data/php.ini
RUN mv /etc/php5/apache2/php.ini /etc/php5/apache2/php.ini.orig && ln -sf /app/data/php.ini /etc/php5/apache2/php.ini

# install RPAF module to override HTTPS, SERVER_PORT, HTTP_HOST based on reverse proxy headers
# https://www.digitalocean.com/community/tutorials/how-to-configure-nginx-as-a-web-server-and-reverse-proxy-for-apache-on-one-ubuntu-16-04-server
RUN mkdir /app/code/rpaf && \
    curl -L https://github.com/gnif/mod_rpaf/tarball/669c3d2ba72228134ae5832c8cf908d11ecdd770 | tar -C /app/code/rpaf -xz --strip-components 1 -f -  && \
    cd /app/code/rpaf && \
    make && \
    make install && \
    rm -rf /app/code/rpaf


# configure rpaf
RUN echo "LoadModule rpaf_module /usr/lib/apache2/modules/mod_rpaf.so" > /etc/apache2/mods-available/rpaf.load && a2enmod rpaf

# phpMyAdmin
RUN mkdir -p /app/code/phpmyadmin && \
    curl -L https://files.phpmyadmin.net/phpMyAdmin/4.9.4/phpMyAdmin-4.9.4-all-languages.tar.gz | tar zxvf - -C /app/code/phpmyadmin --strip-components=1
COPY phpmyadmin-config.inc.php /app/code/phpmyadmin/config.inc.php

# configure cron
RUN rm -rf /var/spool/cron && ln -s /run/cron /var/spool/cron
# clear out the crontab
RUN rm -f /etc/cron.d/* /etc/cron.daily/* /etc/cron.hourly/* /etc/cron.monthly/* /etc/cron.weekly/* && truncate -s0 /etc/crontab

# configure supervisor
ADD supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf

# add code
COPY start.sh index.php crontab.template credentials.template phpmyadmin_login.template /app/code/

# lock www-data but allow su - www-data to work
RUN passwd -l www-data && usermod --shell /bin/bash --home /app/data www-data

# make cloudron exec sane
WORKDIR /app/data

CMD [ "/app/code/start.sh" ]
