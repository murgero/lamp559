#!/bin/bash

## Build app
docker build -t mitchellurgero/org.urgero.lamp559:latest . --no-cache 
docker push mitchellurgero/org.urgero.lamp559:latest

## Install app
cloudron install --image=mitchellurgero/org.urgero.lamp559:latest
